#include <iostream>
#include "json.hpp"
//#include "restclient-cpp/restclient.h"
#include "restclient-cpp/connection.h"

using namespace nlohmann;
int main(int argc, char **argv) {
    std::string WORD_ID = argv[1];
    std::cout << "Looking up " << WORD_ID << " ... " << std::endl;

    std::string ENDPOINT = "entries";
    std::string LANG_CODE = "en-us";
    std::string BASE_URL = "https://od-api.oxforddictionaries.com";
    std::string URI = "/api/v2/" + ENDPOINT + "/" + LANG_CODE + "/" + WORD_ID;

    RestClient::init();
    auto rest = new RestClient::Connection(BASE_URL);
    RestClient::HeaderFields headers;
    headers["app_id"] = "39648f5d";
    headers["app_key"] = "876fc86d13b7f0be60f3f60a18ad4fb9";
    rest->SetHeaders(headers);
    RestClient::Response r = rest->get(URI);

    auto j = json::parse(r.body);
    size_t ctr = 1;
    for (auto a: j["results"]) {
        for (auto b: a["lexicalEntries"]) {
            for (auto c: b["entries"]) {
                for (auto d: c["senses"]) {
                    for (auto e: d["definitions"]) {
                        std::cout << ctr++ << ": " << e << std::endl;
                    }
                }
            }
        }
    }

    return 0;
}
